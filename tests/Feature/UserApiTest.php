<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserApiTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->response = $this->getJson('api/prestador/planos');
        $this->assertApiSuccess();
        $this->dumpApiData();
        $this->logApiData();
    }
}
