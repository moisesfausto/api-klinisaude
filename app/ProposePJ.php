<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProposePJ extends Model
{
    protected $table = "proposta_pj";
    protected $primaryKey = "id_lead";
    public $timestamps = false;

    protected $fillable = [
        'id_operadora',
        'id_lead',
        'num_proposta',
        'cnpj',
        'inscricao_municipal',
        'inscricao_estadual',
        'data_fundacao',
        'razao_social',
        'nome_fantasia',
        'email',
        'telefone',
        'nome_contato',
        'cargo_contato',
        'cep',
        'estado',
        'municipio',
        'bairro',
        'logradouro',
        'numero',
        'complemento',
        'situacao',
        'data_venda',
        'data_cadastro',
				'cod_origem',
				'origem',
    ];

    public function setIdOperadoraAttribute($value)
    {
        $this->attributes['id_operadora'] = intval($value);
    }

    public function setIdLeadAttribute($value)
    {
        $this->attributes['id_lead'] = intval($value);
    }

    public function setNumPropostaAttribute($value)
    {
        $this->attributes['num_proposta'] = intval($value);
    }

    public function setCnpjAttribute($value)
    {
        $this->attributes['cnpj'] = $this->clearField($value);
    }

    public function setDataFundacaoAttribute($value)
    {
        $this->attributes['data_fundacao'] = $this->convertStringToDate($value);
    }

    public function setTelefoneAttribute($value)
    {
        $this->attributes['telefone'] = $this->clearField($value);
    }

    public function setCepAttribute($value)
    {
        $this->attributes['cep'] = $this->clearField($value);
    }

    public function setSituacaoAttribute($value)
    {
        $this->attributes['situacao'] = intval($value);
    }

    public function setDataVendaAttribute($value)
    {
        $this->attributes['data_venda'] = $this->convertStringToDate($value);
    }

    public function setDataCadastroAttribute($value)
    {
        $this->attributes['data_cadastro'] = $this->convertStringToDate($value);
    }

    private function convertStringToDate(?string $param)
    {
        if (empty($param)) {
            return null;
        }

        if (str_contains($param, '-')) {
            return $param;
        }

        list($day, $month, $year) = explode('/', $param);
        return (new \DateTime($year . '-' . $month . '-' . $day))->format('Y-m-d');
    }

    private function clearField(?string $param)
    {
        $remove = [
            '.',
            '-',
            '/',
            '(',
            ')',
            ' ',
        ];

        if (empty($param)) {
            return '';
        }

        return str_replace($remove, '', $param);
    }

}
