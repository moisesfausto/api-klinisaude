<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProposePF extends Model
{
    protected $table = 'proposta_pf';
    protected $primaryKey = 'id_lead';
    public $timestamps = false;

    protected $fillable = [
        'id_operadora',
        'id_lead',
        'num_proposta',
        'cpf_titular',
        'data_nascimento_titular',
        'sexo_titular',
        'nome_titular',
        'nome_mae_titular',
        'email_titular',
        'telefone_titular',
        'cep',
        'estado',
        'municipio',
        'bairro',
        'logradouro',
        'numero',
        'complemento',
        'cpf_resp',
        'data_nascimento_resp',
        'sexo_resp',
        'nome_resp',
        'cod_tipo_dependencia_resp',
        'cod_faixa_etaria',
        'estado_civil',
        'situacao',
        'data_cadastro',
        'cod_operadora_origem',
        'num_contrato_origem',
        'num_registro_ans_origem',
        'data_venda',
        'dt_ini_vig_origem',
        'dt_ultimo_pgto',
        'hash_titular',
				'cod_origem',
				'origem',
    ];

    public function setIdOperadoraAttribute($value)
    {
        $this->attributes['id_operadora'] = intval($value);
    }

    public function setIdLeadAttribute($value)
    {
        $this->attributes['id_lead'] = intval($value);
    }

    public function setNumPropostaAttribute($value)
    {
        $this->attributes['num_proposta'] = intval($value);
    }

    public function setCpfTitularAttribute($value)
    {
        $this->attributes['cpf_titular'] = $this->clearField($value);
    }

    public function setDataNascimentoTitularAttribute($value)
    {
        $this->attributes['data_nascimento_titular'] = $this->convertStringToDate($value);
    }

    public function setTelefoneTitularAttribute($value)
    {
        $this->attributes['telefone_titular'] = $this->clearField($value);
    }

    public function setCepAttribute($value)
    {
        $this->attributes['cep'] = $this->clearField($value);
    }

    public function setCpfRespAttribute($value)
    {
        $this->attributes['cpf_resp'] = $this->clearField($value);
    }

    public function setDataNascimentoRespAttribute($value)
    {
        $this->attributes['data_nascimento_resp'] = $this->convertStringToDate($value);
    }

    public function setCodTipoDependenciaRespAttribute($value)
    {
        $this->attributes['cod_tipo_dependencia_resp'] = intval($value);
    }

    public function setCodFaixaEtariaAttribute($value)
    {
        $this->attributes['cod_faixa_etaria'] = intval($value);
    }

    public function setSituacaoAttribute($value)
    {
        $this->attributes['situacao'] = intval($value);
    }

    public function setDataCadastroAttribute($value)
    {
        $this->attributes['data_cadastro'] = $this->convertStringToDate($value);
    }

    public function setCodOperadoraOrigemAttribute($value)
    {
        $this->attributes['cod_operadora_origem'] = intval($value);
    }

    public function setNumContratoOrigemAttribute($value)
    {
        $this->attributes['num_contrato_origem'] = intval($value);
    }

    public function setNumRegistroAnsOrigemAttribute($value)
    {
        $this->attributes['num_registro_ans_origem'] = intval($value);
    }

    public function setDataVendaAttribute($value)
    {
        $this->attributes['data_venda'] = $this->convertStringToDate($value);
    }

    public function setDtIniVigOrigemAttribute($value)
    {
        $this->attributes['dt_ini_vig_origem'] = $this->convertStringToDate($value);
    }

    public function setDtUltimoPgtoAttribute($value)
    {
        $this->attributes['dt_ultimo_pgto'] = $this->convertStringToDate($value);
    }

    private function convertStringToDate(?string $param)
    {
        if (empty($param)) {
            return null;
        }

        if (str_contains($param, '-')) {
            return $param;
        }

        list($day, $month, $year) = explode('/', $param);
        return (new \DateTime($year . '-' . $month . '-' . $day))->format('Y-m-d');
    }

    private function convertDocument(string $param)
    {
        return substr($param, 0, 3) . '.' . substr($param, 3, 3) . '.' .
            substr($param, 6, 3) . '-' . substr($param, 9, 3);
    }

    private function clearField(?string $param)
    {
        $remove = [
            '.',
            '-',
            '/',
            '(',
            ')',
            ' ',
        ];

        if (empty($param)) {
            return '';
        }

        return str_replace($remove, '', $param);
    }


}
