<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProposePayment extends Model
{
    protected $table = "proposta_pagamento";
    protected $primaryKey = "id_lead";
    public $timestamps = false;

    protected $fillable = [
        'id_operadora',
        'id_lead',
        'cod_tipo_cobranca',
        'nome_cartao',
        'numero_cartao',
        'validade_cartao',
        'cvv_cartao',
        'dia_vencimento'
    ];

    public function setIdOperadoraAttribute($value)
    {
        $this->attributes['id_operadora'] = intval($value);
    }

    public function setIdLeadAttribute($value)
    {
        $this->attributes['id_lead'] = intval($value);
    }

    public function setNomeCartaoAttribute($value)
    {
        $this->attributes['nome_cartao'] = bcrypt($value);
    }

    public function setNumeroCartaoAttribute($value)
    {
        $this->attributes['numero_cartao'] = bcrypt($value);
    }

    public function setValidadeCartaoAttribute($value)
    {
        $this->attributes['validade_cartao'] = bcrypt($value);
    }

    public function setCVVcartaoAttribute($value)
    {
        $this->attributes['cvv_cartao'] = bcrypt($value);
    }
}
