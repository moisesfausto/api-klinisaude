<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProposeDependent extends Model
{
    protected $table = "proposta_pf_dependentes";
    protected $primaryKey = "id_lead";
    public $timestamps = false;

    protected $fillable = [
        'id_operadora',
        'id_lead',
        'num_proposta',
        'id_dependente',
        'cpf_dependente',
        'nome_dependente',
        'cod_tipo_dependencia',
        'nome_mae_dependente',
        'data_nascimento_dependente',
        'sexo_dependente',
        'cod_faixa_etaria',
        'estado_civil',
        'cod_operadora_origem',
        'num_contrato_origem',
        'num_registro_ans_origem',
        'dt_ini_vig_origem',
        'dt_ultimo_pgto',
        'hash',
        'hash_titular'
    ];

    public function setIdOperadoraAttribute($value)
    {
        $this->attributes['id_operadora'] = intval($value);
    }

    public function setIdLeadAttribute($value)
    {
        $this->attributes['id_lead'] = intval($value);
    }

    public function setNumPropostaAttribute($value)
    {
        $this->attributes['num_proposta'] = intval($value);
    }

    public function setCPFDependenteAttribute($value)
    {
        $this->attributes['cpf_dependente'] = $this->clearField($value);
    }

    public function setCodTipoDependenciaAttribute($value)
    {
        $this->attributes['cod_tipo_dependencia'] = intval($value);
    }

    public function setDataNascimentoDependenteAttribute($value)
    {
        $this->attributes['data_nascimento_dependente'] = $this->convertStringToDate($value);
    }

    public function setCodFaixaEtariaAttribute($value)
    {
        $this->attributes['cod_faixa_etaria'] = intval($value);
    }

    public function setCodOperadoraOrigemAttribute($value)
    {
        $this->attributes['cod_operadora_origem'] = intval($value);
    }

    public function setNumContratoOrigemAttribute($value)
    {
        $this->attributes['num_contrato_origem'] = intval($value);
    }

    public function setNumRegistroANSorigemAttribute($value)
    {
        $this->attributes['num_registro_ans_origem'] = intval($value);
    }

    public function setDtIniVigOrigemAttribute($value)
    {
        $this->attributes['dt_ini_vig_origem'] = $this->convertStringToDate($value);
    }

    public function setDtUltimoPgtoAttribute($value)
    {
        $this->attributes['dt_ultimo_pgto'] = $this->convertStringToDate($value);
    }



    private function convertStringToDate(?string $param)
    {
        if (empty($param)) {
            return null;
        }

        if (str_contains($param, '-')) {
            return $param;
        }

        list($day, $month, $year) = explode('/', $param);
        return (new \DateTime($year . '-' . $month . '-' . $day))->format('Y-m-d');
    }

    private function clearField(?string $param)
    {
        $remove = [
            '.',
            '-',
            '/',
            '(',
            ')',
            ' ',
        ];

        if (empty($param)) {
            return '';
        }

        return str_replace($remove, '', $param);
    }
}
