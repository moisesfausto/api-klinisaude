<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Newtwork;

class ProviderController extends Controller
{
    /**
     * @OA\Get(
     *  tags={"Prestador"},
     *  summary="Planos",
     *  description="Obtêm todos os planos da Rede",
     *  path="/api/prestador/planos",
     *  @OA\Response(
     *      response="200",
     *      description="Sucesso"
     * )
     * )
     */
    public function plans()
    {
        $plans = DB::table('rede_completa')
                    ->select('NOM_REDE')
                    ->where('DIVULGA_WEB', 'Sim')
                    ->groupBy('NOM_REDE')
                    ->get();

        return response()->json(['planos' => $plans]);
    }

        /**
     * @OA\Get(
     *  tags={"Prestador"},
     *  summary="Tipo de Prestador",
     *  description="Obtêm todos tipos de Prestadores",
     *  path="/api/prestador/tipo",
     *  @OA\Parameter(
     *      name="plano",
     *      in="query",
     *      required=true,
     *      example="Klini Rio",
     *      @OA\Schema(type="string"),
     *      style="form"
     * ),
     *  @OA\Response(
     *      response="200",
     *      description="Sucesso"
     * )
     * )
     */
    public function type(Request $request)
    {
        $plan = $request->plano;
        $types = DB::table('rede_completa')
                    ->select('NOME_TIPO_PRESTADOR')
                    ->where('NOM_REDE', $plan)
                    ->where('DIVULGA_WEB', 'Sim')
                    ->groupBy('NOME_TIPO_PRESTADOR')
                    ->get();

        return response()->json(['tipos' => $types]);
    }

    /**
     * @OA\Get(
     *  tags={"Prestador"},
     *  summary="Cidades",
     *  description="Obtêm todos as cidades",
     *  path="/api/prestador/cidades",
     *  @OA\Parameter(
     *      name="plano",
     *      in="query",
     *      required=true,
     *      example="Klini Rio",
     *      @OA\Schema(type="string"),
     *      style="form"
     * ),
     * @OA\Parameter(
     *      name="tipo",
     *      in="query",
     *      required=true,
     *      example="Pronto-Atendimento",
     *      @OA\Schema(type="string"),
     *      style="form"
     * ),
     *  @OA\Response(
     *      response="200",
     *      description="Sucesso"
     * )
     * )
     */
    public function citys(Request $request)
    {
        $plan = $request->plano;
        $type = $request->tipo;
        $citys = DB::table('rede_completa')
                    ->select('NOM_MUNICIPIO')
                    ->where('NOM_REDE', $plan)
                    ->where('NOME_TIPO_PRESTADOR', $type)
                    ->groupBy('NOM_MUNICIPIO')
                    ->get();

        return response()->json(['cidades' => $citys]);
    }

    /**
     * @OA\Get(
     *  tags={"Prestador"},
     *  summary="Bairros",
     *  description="Obtêm todos os bairros",
     *  path="/api/prestador/bairros",
     *  @OA\Parameter(
     *      name="plano",
     *      in="query",
     *      required=true,
     *      example="Klini Rio",
     *      @OA\Schema(type="string"),
     *      style="form"
     * ),
     * @OA\Parameter(
     *      name="tipo",
     *      in="query",
     *      required=true,
     *      example="Pronto-Atendimento",
     *      @OA\Schema(type="string"),
     *      style="form"
     * ),
     * @OA\Parameter(
     *      name="cidade",
     *      in="query",
     *      required=true,
     *      example="RIO DE JANEIRO",
     *      @OA\Schema(type="string"),
     *      style="form"
     * ),
     *  @OA\Response(
     *      response="200",
     *      description="Sucesso"
     * )
     * )
     */
    public function neighborhoods(Request $request)
    {
        $city = $request->cidade;
        $plan = $request->plano;
        $type = $request->tipo;
        $neighborhoods = DB::table('rede_completa')
                    ->select('NOM_BAIRRO')
                    ->where('NOM_REDE', $plan)
                    ->where('NOME_TIPO_PRESTADOR', $type)
                    ->where('NOM_MUNICIPIO', $city)
                    ->where('DIVULGA_WEB', 'Sim')
                    ->groupBy('NOM_BAIRRO')
                    ->get();

        return response()->json(['bairros' => $neighborhoods]);
    }

    /**
     * @OA\Get(
     *  tags={"Prestador"},
     *  summary="Especialidades",
     *  description="Obtêm todos as especialidades",
     *  path="/api/prestador/especialidades",
     *  @OA\Parameter(
     *      name="plano",
     *      in="query",
     *      required=true,
     *      example="Klini Rio",
     *      @OA\Schema(type="string"),
     *      style="form"
     * ),
     * @OA\Parameter(
     *      name="tipo",
     *      in="query",
     *      required=true,
     *      example="Pronto-Atendimento",
     *      @OA\Schema(type="string"),
     *      style="form"
     * ),
     * @OA\Parameter(
     *      name="cidade",
     *      in="query",
     *      required=true,
     *      example="RIO DE JANEIRO",
     *      @OA\Schema(type="string"),
     *      style="form"
     * ),
     * @OA\Parameter(
     *      name="bairro",
     *      in="query",
     *      required=true,
     *      example="BARRA DA TIJUCA",
     *      @OA\Schema(type="string"),
     *      style="form"
     * ),
     *  @OA\Response(
     *      response="200",
     *      description="Sucesso"
     * )
     * )
    */
    public function specialty(Request $request)
    {
        $plan = $request->plano;
        $city = $request->cidade;
        $neighborhood = $request->bairro;
        $type = $request->tipo;
        $specialtys = DB::table('rede_completa')
                    ->select('NOME_ESPECIALIDADE')
                    ->where('NOM_REDE', $plan)
                    ->where('NOME_TIPO_PRESTADOR', $type)
                    ->where('NOM_MUNICIPIO', $city)
                    ->where('NOM_BAIRRO', $neighborhood)
                    ->where('DIVULGA_WEB', 'Sim')
                    ->groupBy('NOME_ESPECIALIDADE')
                    ->get();

        return response()->json(['bairros' => $specialtys]);
    }

    /**
     * @OA\Get(
     *  tags={"Prestador"},
     *  summary="Prestadores",
     *  description="Obtêm todos os prestadores - Passar o plano(obrigatorio, e passar os demais parametros sempre em forma de cascata, ex.: plano > cidade || plano > cidade > bairro || plano > cidade > bairro > especialidade || plano > especialidade)",
     *  path="/api/prestadores",
     *  @OA\Parameter(
     *      name="plano",
     *      in="query",
     *      required=true,
     *      example="Klini Rio",
     *      @OA\Schema(type="string"),
     *      style="form"
     * ),
     * @OA\Parameter(
     *      name="tipo",
     *      in="query",
     *      required=false,
     *      example="Pronto-Atendimento",
     *      @OA\Schema(type="string"),
     *      style="form"
     * ),
     * @OA\Parameter(
     *      name="cidade",
     *      in="query",     *
     *      example="RIO DE JANEIRO",
     *      @OA\Schema(type="string"),
     *      style="form"
     * ),
     * @OA\Parameter(
     *      name="bairro",
     *      in="query",     *
     *      example="BARRA DA TIJUCA",
     *      @OA\Schema(type="string"),
     *      style="form"
     * ),
     * @OA\Parameter(
     *      name="especialidade",
     *      in="query",     *
     *      example="Emergência Adulto",
     *      @OA\Schema(type="string"),
     *      style="form"
     * ),
     *  @OA\Response(
     *      response="200",
     *      description="Sucesso"
     * ),
     *  @OA\Response(
     *      response="404",
     *      description="Não encontrado"
     * )
     * )
    */
    public function provider(Request $request)
    {
        $plan = $request->plano;
        $city = $request->cidade;
        $neighborhood = $request->bairro;
        $specialty = $request->especialidade;
        $type = $request->tipo;

        if ($plan && $specialty) {
            $providers = DB::table('rede_completa')
                        ->select('NOME_TIPO_PRESTADOR','NOME_PRESTADOR','NUM_DDD_TELEFONE_1','NUM_TELEFONE_1','NUM_TELEFONE_2',
                        'NOME_ESPECIALIDADE','COD_TIPO_LOGR','END_PRESTADOR','NUM_ENDERECO','NUM_CEP','NOM_BAIRRO',
                        'NOM_MUNICIPIO')
                        ->where('DIVULGA_WEB', 'Sim')
                        ->where('NOM_REDE', $plan)
                        ->where('NOME_ESPECIALIDADE', $specialty)
                        ->groupBy('NOME_ESPECIALIDADE', 'END_PRESTADOR','NOME_PRESTADOR')
                        ->get();
        }

        if ($plan) {
            $providers = DB::table('rede_completa')
                        ->select('NOME_TIPO_PRESTADOR','NOME_PRESTADOR','NUM_DDD_TELEFONE_1','NUM_TELEFONE_1','NUM_TELEFONE_2',
                        'NOME_ESPECIALIDADE','COD_TIPO_LOGR','END_PRESTADOR','NUM_ENDERECO','NUM_CEP','NOM_BAIRRO',
                        'NOM_MUNICIPIO', 'NOM_REDE')
                        ->where('DIVULGA_WEB', 'Sim')
                        ->where('NOM_REDE', $plan)
                        ->groupBy('NOME_ESPECIALIDADE', 'END_PRESTADOR','NOME_PRESTADOR')
                        ->get();
        }

        if ($plan && $type) {
            $providers = DB::table('rede_completa')
                        ->select('NOME_TIPO_PRESTADOR','NOME_PRESTADOR','NUM_DDD_TELEFONE_1','NUM_TELEFONE_1','NUM_TELEFONE_2',
                        'NOME_ESPECIALIDADE','COD_TIPO_LOGR','END_PRESTADOR','NUM_ENDERECO','NUM_CEP','NOM_BAIRRO',
                        'NOM_MUNICIPIO')
                        ->where('DIVULGA_WEB', 'Sim')
                        ->where('NOM_REDE', $plan)
                        ->where('NOME_TIPO_PRESTADOR', $type)
                        ->groupBy('NOME_ESPECIALIDADE', 'END_PRESTADOR','NOME_PRESTADOR')
                        ->get();
        }

        if ($plan && $type && $city) {
            $providers = DB::table('rede_completa')
                        ->select('NOME_TIPO_PRESTADOR','NOME_PRESTADOR','NUM_DDD_TELEFONE_1','NUM_TELEFONE_1','NUM_TELEFONE_2',
                        'NOME_ESPECIALIDADE','COD_TIPO_LOGR','END_PRESTADOR','NUM_ENDERECO','NUM_CEP','NOM_BAIRRO',
                        'NOM_MUNICIPIO')
                        ->where('DIVULGA_WEB', 'Sim')
                        ->where('NOM_REDE', $plan)
                        ->where('NOME_TIPO_PRESTADOR', $type)
                        ->where('NOM_MUNICIPIO', $city)
                        ->groupBy('NOME_ESPECIALIDADE', 'END_PRESTADOR','NOME_PRESTADOR')
                        ->get();
        }

        if ($plan && $type && $city && $neighborhood) {
            $providers = DB::table('rede_completa')
                        ->select('NOME_TIPO_PRESTADOR','NOME_PRESTADOR','NUM_DDD_TELEFONE_1','NUM_TELEFONE_1','NUM_TELEFONE_2',
                        'NOME_ESPECIALIDADE','COD_TIPO_LOGR','END_PRESTADOR','NUM_ENDERECO','NUM_CEP','NOM_BAIRRO',
                        'NOM_MUNICIPIO')
                        ->where('DIVULGA_WEB', 'Sim')
                        ->where('NOM_REDE', $plan)
                        ->where('NOME_TIPO_PRESTADOR', $type)
                        ->where('NOM_MUNICIPIO', $city)
                        ->where('NOM_BAIRRO', $neighborhood)
                        ->groupBy('NOME_ESPECIALIDADE', 'END_PRESTADOR','NOME_PRESTADOR')
                        ->get();
        }

        if ($plan && $city && $neighborhood && $specialty && $type) {
            $providers = DB::table('rede_completa')
                        ->select('NOME_TIPO_PRESTADOR','NOME_PRESTADOR','NUM_DDD_TELEFONE_1','NUM_TELEFONE_1','NUM_TELEFONE_2',
                        'NOME_ESPECIALIDADE','COD_TIPO_LOGR','END_PRESTADOR','NUM_ENDERECO','NUM_CEP','NOM_BAIRRO',
                        'NOM_MUNICIPIO')
                        ->where('DIVULGA_WEB', 'Sim')
                        ->where('NOM_REDE', $plan)
                        ->where('NOME_TIPO_PRESTADOR', $type)
                        ->where('NOM_MUNICIPIO', $city)
                        ->where('NOM_BAIRRO', $neighborhood)
                        ->where('NOME_ESPECIALIDADE', $specialty)
                        ->groupBy('NOME_ESPECIALIDADE','END_PRESTADOR','NOME_PRESTADOR')
                        ->get();
        }

        if (empty($providers->all())) {
            $json = [
                'message' => 'Not Found',
                'code' => '404'
            ];
        }

        if (!empty($providers->all())) {
            $json = [
                'message' => 'Success',
                'code' => '202',
                'prestadores' => $providers
            ];
        }

        return response()->json($json);
    }
}
