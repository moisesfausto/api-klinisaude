<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\ProposeDependent;
use App\ProposeLife;
use App\ProposePayment;
use App\ProposePF;
use App\ProposePJ;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use mysql_xdevapi\Table;

class ProposalController extends Controller
{

    public function leads()
    {

        $leads = DB::table('lead')
            ->join('proposta_pf_dependentes', 'lead.id_lead', '=', 'proposta_pf_dependentes.id_lead')
            ->whereRaw('lead.hash = proposta_pf_dependentes.hash_titular')
            ->join('lead_plano', 'lead_plano.id_lead', '=', 'lead.id_lead')
            ->select('*')
            ->get();

        return response()->json(['data' => $leads]);

    }

    public function lead($id_lead)
    {
        $lead = DB::table('lead')
            ->join('proposta_pf_dependentes', 'lead.id_lead', '=', 'proposta_pf_dependentes.id_lead')
            ->join('lead_plano', 'lead.id_lead', '=', 'lead_plano.id_lead')
            ->whereRaw('lead.hash = proposta_pf_dependentes.hash_titular AND lead.id_lead = ?', [$id_lead])
            ->select('*')
            ->first();

        return response()->json(['data' => $lead]);
    }

		public function proposal(int $id_lead = null, String $type = '', int $cod_origem = null)
		{
			if (!$id_lead || !$type || !$cod_origem) {
				$json = [
					'return' => 'false',
					'message' => 'Nenhum parametro informado!',
					'example' => 'proposal/{id_lead}/{pf_pj}/{cod_origem}',
				];
				return $json;
			}

			if ($type == 'pf') {
				$propose = DB::table('proposta_pf')
					->where('proposta_pf.id_lead', $id_lead)
					->where('proposta_pf.cod_origem', $cod_origem)
					->select('*')
					->first();
			}

			if ($type == 'pj') {
				$propose = DB::table('proposta_pj')
					->where('proposta_pj.id_lead', $id_lead)
					->where('proposta_pj.cod_origem', $cod_origem)
					->select('*')
					->first();
			}

			return response()->json(['data' => $propose]);
    }

		public function proposalOrigin(int $cod_origem, string $type = null)
		{
			if ($type == 'pf') {
				$propose = DB::table('proposta_pf')
					->where('proposta_pf.cod_origem', $cod_origem)
					->select('*')
					->get();
			} elseif ( $type == 'pj' ) {
				$propose = DB::table('proposta_pj')
					->where('proposta_pj.cod_origem', $cod_origem)
					->select('*')
					->get();
			}

			return response()->json(['data' => $propose]);
    }

    public function situations()
    {
        $situations = DB::table('lead')
            ->join('proposta_pf', 'proposta_pf.id_lead', '=', 'lead.id_lead')
            ->join('situacao_proposta', 'proposta_pf.situacao', '=', 'situacao_proposta.situacao')
            ->whereRaw('lead.hash = proposta_pf.hash_titular')
            ->select('proposta_pf.id_lead', 'lead.nome', 'lead.email', 'lead.telefone', 'lead.data_nascimento',
                'situacao_proposta.nome_situacao')
            ->get();
        return response()->json(['data' => $situations]);
    }

    public function situation($id_lead)
    {
         $situation = DB::table('lead')
            ->join('proposta_pf', 'lead.id_lead', '=', 'proposta_pf.id_lead')
            ->join('situacao_proposta', 'situacao_proposta.situacao', '=', 'proposta_pf.situacao')
            ->whereRaw('lead.hash = proposta_pf.hash_titular AND proposta_pf.id_lead = ?', [$id_lead])
            ->select('proposta_pf.id_lead', 'lead.nome', 'lead.email', 'lead.telefone', 'lead.data_nascimento',
                'situacao_proposta.nome_situacao')
            ->first();

         if (!$situation) {
             $json = [
                 'return' => false,
                 'message' => 'Situação não encontrada!'
             ];

             return $json;
         }

        return response()->json(['data' => $situation]);
    }

    public function storeProposePf(Request $request)
    {
        $propose = new ProposePF();

        $existPropose =  ProposePF::where('id_lead', $request->id_lead)->first();
        if (isset($existPropose)) {

            $json = [
                'return' => false,
                'message' => 'Proposta já cadastrada.'
            ];

            return response()->json($json);

        }

        $propose->fill($request->all());

        $propose->setIdOperadoraAttribute($request->id_operadora);
        $propose->setIdLeadAttribute($request->id_lead);
        $propose->setNumPropostaAttribute($request->num_proposta);
        $propose->setCpfTitularAttribute($request->cpf_titular);
        $propose->setDataNascimentoTitularAttribute($request->data_nascimento_titular);
        $propose->setTelefoneTitularAttribute($request->telefone_titular);
        $propose->setCepAttribute($request->cep);
        $propose->setCpfRespAttribute($request->cpf_resp);
        $propose->setDataNascimentoRespAttribute($request->data_nascimento_resp);
        $propose->setCodTipoDependenciaRespAttribute($request->cod_tipo_dependencia_resp);
        $propose->setCodFaixaEtariaAttribute($request->cod_faixa_etaria);
        $propose->setSituacaoAttribute($request->situacao);
        $propose->setDataCadastroAttribute($request->data_cadastro);
        $propose->setCodOperadoraOrigemAttribute($request->cod_operadora_origem);
        $propose->setNumContratoOrigemAttribute($request->num_contrato_origem);
        $propose->setNumRegistroAnsOrigemAttribute($request->num_registro_ans_origem);
        $propose->setDataVendaAttribute($request->data_venda);
        $propose->setDtIniVigOrigemAttribute($request->dt_ini_vig_origem);
        $propose->setDtUltimoPgtoAttribute($request->dt_ultimo_pgto);

        $propose->save();

        $json = [
            'return' => true,
            'message' => 'Proposta cadastrada com sucesso!',
        ];

        return response()->json($json);
    }

    public function storeProposalPj(Request $request)
    {
        $propose = new ProposePJ();

        $existPropose =  ProposePJ::where('id_lead', $request->id_lead)->first();
        if (isset($existPropose)) {

            $json = [
                'return' => false,
                'message' => 'Proposta já cadastrada.'
            ];

            return response()->json($json);

        }

        $propose->fill($request->all());
        $propose->setIdOperadoraAttribute($request->id_operadora);
        $propose->setIdLeadAttribute($request->id_lead);
        $propose->setNumPropostaAttribute($request->num_proposta);
        $propose->setCnpjAttribute($request->cnpj);
        $propose->setDataFundacaoAttribute($request->data_fundacao);
        $propose->setTelefoneAttribute($request->telefone);
        $propose->setCepAttribute($request->cep);
        $propose->setSituacaoAttribute($request->situacao);
        $propose->setDataVendaAttribute($request->data_venda);
        $propose->setDataCadastroAttribute($request->data_cadastro);

        $propose->save();

        $json = [
            'return' => true,
            'message' => 'Proposta cadastrada com sucesso!',
        ];

        return response()->json($json);

    }

    public function storeProposalDependent(Request $request)
    {
        $proposal = new ProposeDependent();

        $proposal->fill($request->all());

        $proposal->setIdOperadoraAttribute($request->id_operadora);
        $proposal->setIdLeadAttribute($request->id_lead);
        $proposal->setNumPropostaAttribute($request->num_proposta);
        $proposal->setCPFDependenteAttribute($request->cpf_dependente);
        $proposal->setCodTipoDependenciaAttribute($request->cod_tipo_dependencia);
        $proposal->setDataNascimentoDependenteAttribute($request->data_nascimento_dependente);
        $proposal->setCodFaixaEtariaAttribute($request->cod_faixa_etaria);
        $proposal->setCodOperadoraOrigemAttribute($request->cod_operadora_origem);
        $proposal->setNumContratoOrigemAttribute($request->num_contrato_origem);
        $proposal->setNumRegistroANSorigemAttribute($request->num_registro_ans_origem);
        $proposal->setDtIniVigOrigemAttribute($request->dt_ini_vig_origem);
        $proposal->setDtUltimoPgtoAttribute($request->dt_ultimo_pgto);

        $proposal->save();

        $json = [
            'return' => true,
            'message' => 'Dependente cadastrado com sucesso!',
        ];

        return response()->json($json);

    }

    public function storeProposalPayment(Request $request)
    {
        $proposal = new ProposePayment();

        $proposal->fill($request->all());

        $proposal->setIdOperadoraAttribute($request->id_operadora);
        $proposal->setIdLeadAttribute($request->id_lead);
        $proposal->setNomeCartaoAttribute($request->nome_cartao);
        $proposal->setNumeroCartaoAttribute($request->numero_cartao);
        $proposal->setValidadeCartaoAttribute($request->validade_cartao);
        $proposal->setCVVcartaoAttribute($request->cvv_cartao);

        $proposal->save();

        $json = [
            'return' => true,
            'message' => 'Dados de pagamento cadastrado com sucesso!',
        ];

        return response()->json($json);

    }

    public function storeProposalLife(Request $request)
    {
        $proposal = new ProposeLife();

        $proposal->fill($request->all());
        $proposal->setIdLeadAttribute($request->id_lead);
        $proposal->setIdDependeteAttribute($request->id_dependente);
        $proposal->setImgProvaAttribute($request->img_prova);
        $proposal->setTipoComprovanteAttribute($request->tipo_comprovante);
        $proposal->setDatacadastroAttribute($request->datacadastro);

        if ($request->hasFile('img_prova')) {
        	if ($request->file('img_prova')->isValid()) {
						$request->file('img_prova')->storeAs('upload', $proposal->img_prova, 'ftp');
					} else {
        		return 'Documento com formato inválido!';
					}
				} else {
        	return 'Documento não existe!';
				}

        $proposal->save();

				$json = [
					'return' => true,
					'message' => 'Documentos foram salvos com sucesso!',
				];

				return response()->json($json);

    }

    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function updatePropose(Request $request, $entity, $id)
    {
        if ($entity == 'pf') {
            $propose = ProposePF::find($id);
        }

        if ($entity == 'pj') {
            $propose = ProposePJ::find($id);
        }

        if (!$propose) {
            $json = [
                'return' => false,
                'message' => 'Proposta não encontrada!'
            ];

            return response()->json($json);
        }

        $propose->setSituacaoAttribute($request->situacao);
        $propose->save();

        $json = [
            'return' => true,
            'message' => 'Situação da proposta alterado com sucesso!'
        ];

        return response()->json($json);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
