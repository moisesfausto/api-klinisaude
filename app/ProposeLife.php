<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProposeLife extends Model
{
    protected $table = "proposta_pf_prova_de_vida";
    protected $primaryKey = "id_prova";
    public $timestamps = false;

    protected $fillable = [
        'id_lead',
        'id_dependente',
        'img_prova',
        'tipo_comprovante',
        'datacadastro',
        'hash',
    ];

    public function setIdLeadAttribute($value)
    {
        $this->attributes['id_lead'] = intval($value);
    }

    public function setIdDependeteAttribute($value)
    {
        $this->attributes['id_dependente'] = intval($value);
    }

    public function setImgProvaAttribute($value)
    {
        $dir = 'upload/';
        $slug = date('YmdHis') . 'selfie' . $this->attributes['id_lead'];
        $ext = $value->extension();
        $nameArchive = $slug . '.' . $ext;

        $this->attributes['img_prova'] = $nameArchive;

        //$path = $value->file($nameArchive)->store($dir);
    }

    public function setTipoComprovanteAttribute($value)
    {
        $this->attributes['tipo_comprovante'] = intval($value);
    }

    public function setDatacadastroAttribute($value)
    {
        $this->attributes['datacadastro'] = $this->convertStringToDate($value);
    }

    private function convertStringToDate(?string $param)
    {
        if (empty($param)) {
            return null;
        }

        if (str_contains($param, '-')) {
            return $param;
        }

        list($day, $month, $year) = explode('/', $param);
        return (new \DateTime($year . '-' . $month . '-' . $day))->format('Y-m-d');
    }
}
