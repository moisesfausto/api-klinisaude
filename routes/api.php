<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::post('auth/login', 'Api\\AuthController@login');

// Route::group(['middleware' => ['apiJWT']], function () {

//     Route::post('auth/logout', 'Api\\AuthController@logout');

//     // Venda online GET
// 		/*Lead*/
// 		Route::get('lead/{id_lead}', 'Api\\ProposalController@lead');
//     Route::get('leads', 'Api\\ProposalController@leads');
// 		/*Propostas*/
//     Route::get('proposal/{id_lead?}/{type?}/{cod_origem?}', 'Api\\ProposalController@proposal');
//     Route::get('proposal-origin/{cod_origem}/{type?}', 'Api\\ProposalController@proposalOrigin');
// 		/*Situações*/
//     Route::get('situations', 'Api\\ProposalController@situations');
//     Route::get('situation/{id_lead}', 'Api\\ProposalController@situation');

//     // Venda online POST
//     Route::post('store-proposal-pf', 'Api\\ProposalController@storeProposePf');
//     Route::post('store-proposal-pj', 'Api\\ProposalController@storeProposalPj');
//     Route::post('store-proposal-dependent', 'Api\\ProposalController@storeProposalDependent');
//     Route::post('store-proposal-payment', 'Api\\ProposalController@storeProposalPayment');
//     Route::post('store-proposal-proof-life', 'Api\\ProposalController@storeProposalLife');

//     // Venda online PUT
//     Route::put('update-proposal/{entity}/{id_lead}', 'Api\\ProposalController@updatePropose');

// });

// Rede Credenciada //

// Get Plans
Route::get('prestador/planos', 'Api\\ProviderController@plans');
Route::get('prestador/tipo', 'Api\\ProviderController@type');
Route::get('prestador/cidades', 'Api\\ProviderController@citys');
Route::get('prestador/bairros', 'Api\\ProviderController@neighborhoods');
Route::get('prestador/especialidades', 'Api\\ProviderController@specialty');
Route::get('prestadores', 'Api\\ProviderController@provider');
