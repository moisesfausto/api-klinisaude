
# API Klini Saude

Aplicação feita para busca da rede credenciada do plano de saúde


## Installation

Clone o repósitorio do Gitlab
```bash
git clone https://gitlab.com/moisesfausto/api-klinisaude.git
```

Entre na pasta do projeto
```bash
cd API-KLINISAUDE
```

Crie o arquivo .env
Entre na pasta do projeto
```bash
cp .env-example .env
```

Instale ou atualize as depências
Entre na pasta do projeto
```bash
php composer update
```

Gere a KEY
```bash
php artisan key:generate
```
## Running

Para rodar a aplição

```bash
  php artisan serve
```


## Documentation

[Documentação](https://api.klinisaude.com.br/api/docs)


## Tech Stack

**Server:** PHP, Laravel, MySQL


## Authors

- [@moisesfausto](https://gitlab.com/moisesfausto)


## License

[MIT](https://choosealicense.com/licenses/mit/)

